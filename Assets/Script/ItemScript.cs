﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemScript : MonoBehaviour
{
    public Text countText; //収集したアイテムの数を表示する
    public Text clearText; //クリア時のテキストを表示する
    public int count;     //集めたアイテムの数を格納する

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        SetCountText();
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString(); //エサの得点次第でcountに数字をかける
　
        //if (count >= 12) エサの数によって書き換える
        //{
            //clearText.text = "Stage Clear!";
        //}
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
