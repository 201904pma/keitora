﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerItemScript : MonoBehaviour
{
    public GameObject RedEnemy1;
    public GameObject RedEnemy2;
    public GameObject RandomEnemy;
    public GameObject PinkEnemy;
    public Material[] _material;
    private Renderer _renderer;
    private SphereCollider _sphereCollider;
    private GameObject Player;
    public Material ChangeColor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //プレイヤーがパワーエサを取得したとき
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            StartCoroutine("Muteki",900);

            //パワーエサを見えなくする
            _renderer = this.GetComponent<Renderer>();
            _renderer.enabled = false;
            _sphereCollider = this.GetComponent<SphereCollider>();
            _sphereCollider.enabled = false;
        }
    }

    IEnumerator Muteki(int frame)
    {
        while (true)
        {
            // secondで指定した秒数ループします
            yield return null;

            //敵の逃走メソッドを呼び出す (敵の構成は暫定的)
            RedEnemy1.GetComponent<RedEnemyMove>().Escape();
            RedEnemy2.GetComponent<RedEnemyMove>().Escape();
            RandomEnemy.GetComponent<RandomEnemyMove>().Escape();
            PinkEnemy.GetComponent<PinkEnemyMove>().Escape();

            //プレイヤーの色を変える
            Player = GameObject.Find("Player");
            Player.GetComponent<Renderer>().material.color = ChangeColor.color;

            //通常時でも無敵状態でも敵はプレイヤーに当たると自爆

            frame--;
        }
    }
}
