﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public AudioClip HitSE;
    AudioSource aud;

    private float speed;
    public float maxSpeed = 10.0f;
    public float boost = 1.0f;
    private float tireAngre = 0;
    private float maxTireAngreR = 45.0f;
    private float maxTireAngreL = -45.0f;
    public float steering = 3;
    public float grip = 2.0f;
    private float slip = 0;
    public float brakePower = 1.0f;
    public float maxBrakeForce = 10.0f;
    private float brakeForce = 0;
    private bool tireLook = false;
    public float backMaxSpeed = -3.0f;

    private int life = 3;
    GameObject director;

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "oukan")
        {
            Destroy(collision.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        this.aud = GetComponent<AudioSource>();
        this.director = GameObject.Find("GameDirector");
    }

    // Update is called once per frame
    void Update()
    {
        //アクセル
        if (Input.GetKey(KeyCode.Space) && this.tireLook == false)
        {
            if(this.speed < this.maxSpeed)
            {
                this.speed = this.speed + (0.001f * this.boost);
            }
            if (this.speed > this.maxSpeed)
            {
                this.speed = this.maxSpeed;
            }
        }
        //アクセル離す
        if (Input.GetKeyUp(KeyCode.Space) && this.speed >= 0)
        {
            if(this.speed > 0)
            {
                this.speed = this.speed - this.speed * 0.5f - this.speed * 0.2f;
                if(this.speed < 0.04f)
                {
                    this.speed = 0;
                }
            }
            if (this.slip < 0)
            {
                this.slip = this.slip + 0.5f;
                if (this.slip > 0)
                {
                    this.slip = 0;
                }
            }
            if (this.slip > 0)
            {
                this.slip = this.slip - 0.5f;
                if (this.slip < 0)
                {
                    this.slip = 0;
                }
            }
        }

        //バック
        if (Input.GetKey(KeyCode.X) && this.tireLook == false)
        {
            if (this.speed > this.backMaxSpeed)
            {
                this.speed = this.speed - (0.0008f * this.boost);
            }
            if (this.speed < backMaxSpeed)
            {
                this.speed = this.backMaxSpeed;
            }
        }

        //バック離す
        if (Input.GetKeyUp(KeyCode.X) && this.speed <= 0)
        {
            if (this.speed < 0)
            {
                this.speed = this.speed + this.speed / 2;
                if (this.speed < -0.04f)
                {
                    this.speed = 0;
                }
            }
            if (this.slip > 0)
            {
                this.slip = this.slip + 0.1f;
                if (this.slip > 0)
                {
                    this.slip = 0;
                }
            }
            if (this.slip > 0)
            {
                this.slip = this.slip - 0.5f;
                if (this.slip < 0)
                {
                    this.slip = 0;
                }
            }
        }

        //右曲がり
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (this.tireAngre < this.maxTireAngreR)
            {
                this.tireAngre = this.tireAngre + (this.steering * 0.01f);
            }
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (this.tireAngre > 0)
            {
                this.tireAngre = this.tireAngre-2.5f;
                if (this.tireAngre < 0)
                {
                    this.tireAngre = 0;
                }
            }
        }
        //左曲がり
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (this.tireAngre > this.maxTireAngreL)
            {
                this.tireAngre = this.tireAngre - (this.steering * 0.01f);
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (this.tireAngre < 0)
            {
                this.tireAngre = this.tireAngre+2.5f;
                if (this.tireAngre > 0)
                {
                    this.tireAngre = 0;
                }
            }
        }
        //曲がる方向
        if (this.speed > 0.01f)
        {
            transform.Rotate(0, this.tireAngre, 0);
            if (tireAngre == 0)
            {
                this.slip = 0;
            }
            else
            {
                this.slip = speed / (-5.5f * this.grip) * tireAngre;
            }
            transform.Translate(this.slip, 0, 0);
            if (this.slip < 0)
            {
                this.slip = this.slip + 0.1f;
                if (this.slip > 0)
                {
                    this.slip = 0;
                }
            }
            if (this.slip > 0)
            {
                this.slip = this.slip - (3.0f / this.speed);
                if (this.slip < 0)
                {
                    this.slip = 0;
                }
            }
        }


        //ブレーキ
        if (Input.GetKey(KeyCode.Z))
        {
            this.tireLook = true;
            if(this.speed > 0)
            {
                if (this.brakeForce < this.maxBrakeForce)
                {
                    this.brakeForce = this.brakeForce + this.brakePower;
                }
                this.speed = this.speed - (brakeForce * 0.001f);
                if (this.speed < 0)
                {
                    this.speed = 0;
                    this.brakeForce = 0;
                }
            }

            if (this.speed < 0)
            {
                if (this.brakeForce < this.maxBrakeForce)
                {
                    this.brakeForce = this.brakeForce + this.brakePower;
                }
                this.speed = this.speed + (brakeForce * 0.001f);
                if (this.speed > 0)
                {
                    this.speed = 0;
                    this.brakeForce = 0;
                }
            }
        }
        //ブレーキ離す
        if (Input.GetKeyUp(KeyCode.Z))
        {
            this.tireLook = false;
            if (this.brakeForce > 0)
            {
                this.brakeForce = this.brakeForce - 0.1f;
                if (brakeForce < 0)
                {
                    brakeForce = 0;
                }
            }
        }



            //スピードの分だけ加速
            transform.Translate(0, 0, this.speed);

            //スピードが０の時スリップが０に
            if (this.speed==0)
            {
                this.slip = 0;
            }
            if (this.slip > 0)
            {
            this.slip = this.slip - this.speed/this.grip/2;
            }
            if (this.slip < 0)
            {
                this.slip = this.slip + this.speed / this.grip / 2;
            }
    }

    void OnCollisionEnter(Collision other)
    {
        //壁激突
        if(other.gameObject.tag =="wall")
        {
            //this.aud.PlayOneShot(this.HitSE);
            this.slip = this.slip/2.0f;
            this.tireAngre = 0;
            if (this.speed > 0)
            {
                this.speed = this.speed / 1.1f - 0.2f;
                if (this.speed < 0)
                {
                    this.speed = 0;
                }
                //transform.Translate(0, 0,-0.5f);
            }
            if (other.gameObject.tag == "stage")
            {
                this.speed = 0;
                this.slip = 0;
                this.tireAngre = 0;
                transform.Translate(0, 100.0f, 0);
            }
        }
        //敵と衝突
        if (other.gameObject.tag == "enemy")
        {
            this.life -= 1;
            int health = this.life;
            Debug.Log(this.life);
            director.GetComponent<GameDirector>().SetPlayerHealthUI(health);
            Destroy(other.gameObject);
        }
    }
}
