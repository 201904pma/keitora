﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{

    GameObject[] tagObjects;
    GameObject countText;

    float timer = 0.0f;
    float interval = 2.0f;
    int count = 0;

    public GameObject Life1; // プレイヤー残り体力1を示すUI
    public GameObject Life2; // プレイヤー残り体力2を示すUI
    public GameObject Life3; // プレイヤー残り体力3を示すUI

        // Start is called before the first frame update
        void Start()
    {
        this.countText = GameObject.Find("Count");
    }

    // Update is called once per frame
    void Update()
    {
            Check("oukan");
    }
    void Check(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);
        this.count = tagObjects.Length;
        this.countText.GetComponent<Text>().text =
            this.count.ToString();

        if (tagObjects.Length == 0)
        {
            SceneManager.LoadScene("ClearScene");
        }
    }



    public void SetPlayerHealthUI(int health)
    {
        // 残り体力によって非表示にすべき体力アイコンを消去する
        if (health == 2)
        { // 体力2になった場合
            Destroy(Life3); // 3つめのアイコンを消去
        }
        else if (health == 1)
        { // 体力1になった場合
            Destroy(Life2); // 2つめのアイコンを消去
        }
        else if (health == 0)
        { // 体力0になった場合
            Destroy(Life1); // 1つめのアイコンを消去
            SceneManager.LoadScene("GameOverScene"); 
        }
    }
}
