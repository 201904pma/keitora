﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//ドロップボックス　UnityUi　参照
public class TitleScript : MonoBehaviour
{    
    public void ChangeScene(string scene)
    {
        switch (scene)
        {
            case "Title":
                //タイトル画面に移動
                SceneManager.LoadScene("TitleScene");
                break;
            case "Atagi_Scenes2":
                //レースシーンに移動(みんなが作ったシーンに変更)
                SceneManager.LoadScene("Atagi_Scenes3");
                break;
            case "HowToScene":
                SceneManager.LoadScene("HowToScene");
                break;
            default:
                //タイトル画面に移動
                SceneManager.LoadScene("TitleScene");
                break;
        }
    }
}
