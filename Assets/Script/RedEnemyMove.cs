﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Timers;

public class RedEnemyMove : EnemysNavMesh
{
    public float changeTarget = 30f;
    private Vector3 pos;
    public float speed = 3.5f;
    private NavMeshAgent agent;
    int time = 0;
    public GameObject particlePrefab;

    // タイマーの間隔(ミリ秒)
    Timer timer = new Timer(1000);

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        GameObject target = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(target.position);
    }

    public void Escape()
    {
        //プレイヤーの座標を取得
        Vector3 targetPos = GameObject.Find("Player").transform.position;

        //この座標へ移動する
        agent.SetDestination(targetPos * -1);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //GenerateParticle();
            Destroy(this.gameObject);
        }
    }

    public void GenerateParticle()
    {
        //パーティクルエフェクトを生み出す
        GameObject pp = Instantiate(particlePrefab) as GameObject;

        //敵の位置にパーティクルを置く
        pp.transform.position = gameObject.transform.position;

        //エフェクト発生後パーティクル削除
        Destroy(pp, 2.0f);
    }
}
