﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Escape : MonoBehaviour
{
    private Vector3 nlz;
    public GameObject target;
    private NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Move()
    {
        //自分の座標を取得
        //Vector3 myPos = this.transform.position;

        //プレイヤーの座標を取得
        Vector3 targetPos = GameObject.Find("Player").transform.position;

        //プレイヤー座標から敵座標を引いて正規化
        //Vector3 pos = myPos - targetPos;
        //Vector3 nlz = pos.normalized;

        //この座標へ移動する
        agent.SetDestination(targetPos * -1);
    }
}
