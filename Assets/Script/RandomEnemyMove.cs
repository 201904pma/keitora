﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using System.Timers;

public class RandomEnemyMove : EnemysNavMesh
{
    static Vector3 pos;
    NavMeshAgent agent;
    private Vector3 nlz;
    int time = 0;
    public GameObject particlePrefab;

    // タイマーの間隔(ミリ秒)
    Timer timer = new Timer(1000);

    float agentToPatroldistance;
    float agentToTargetdistance;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }


    void Start()
    {
        GameObject target = GameObject.Find("Player");
        DoPatrol();
    }


    void Update()
    {
        //Agentと目的地の距離
        agentToPatroldistance = Vector3.Distance(this.agent.transform.position, pos);

        //Agentとプレイヤーの距離
        agentToTargetdistance = Vector3.Distance(this.agent.transform.position, target.transform.position);


        //プレイヤーとAgentの距離が30f以下になると追跡開始
        if (agentToTargetdistance <= 30.0f)
        {
            DoTracking();

            //プレイヤーと目的地の距離が15f以下になると次の目的地をランダム指定
        }
        else if (agentToPatroldistance < 15.0f)
        {
            DoPatrol();
        }
    }

    //エージェントが向かう先をランダムに指定するメソッド
    public void DoPatrol()
    {
        var x = Random.Range(-50.0f, 50.0f);
        var z = Random.Range(-50.0f, 50.0f);
        pos = new Vector3(x, 0, z);
        agent.SetDestination(pos);
    }

    //targetに指定したplayerを追いかけるメソッド
    public void DoTracking()
    {
        pos = target.position;
        agent.SetDestination(pos);
    }

    public void Escape()
    {
        //プレイヤーの座標を取得
        Vector3 targetPos = GameObject.Find("Player").transform.position;

        //この座標へ移動する
        agent.SetDestination(targetPos * -1);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //GenerateParticle();
            Destroy(this.gameObject);
        }
    }

    public void GenerateParticle()
    {
        //パーティクルエフェクトを生み出す
        GameObject pp = Instantiate(particlePrefab) as GameObject;

        //敵の位置にパーティクルを置く
        pp.transform.position = gameObject.transform.position;

        //エフェクト発生後パーティクル削除
        Destroy(pp, 2.0f);
    }
}
