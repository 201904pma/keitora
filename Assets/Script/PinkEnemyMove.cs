﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Timers;

public class PinkEnemyMove : EnemysNavMesh
{
    private float mySpeed;
    private Vector3 myLatestPos;
    Rigidbody rigid;
    private float targetSpeed;
    NavMeshAgent agent;
    private Vector3 nlz;
    int time = 0;
    public GameObject particlePrefab;

    // タイマーの間隔(ミリ秒)
    Timer timer = new Timer(1000);

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        GameObject target = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Intercept();
    }

    void Intercept()
    {
        //自分の座標を取得
        Vector3 myPos = this.transform.position;

        //プレイヤーの座標を取得
        Vector3 targetPos = GameObject.Find("Player").transform.position;

        //自分から見た相対距離を取得
        float relativeDistance = (targetPos - myPos).magnitude;

        //自分の速度を取得
        mySpeed
  = ((this.transform.position - myLatestPos) / Time.deltaTime).magnitude;
        myLatestPos = this.transform.position;

        //プレイヤーの速度を取得
        rigid = GameObject.Find("Player").GetComponent<Rigidbody>();
        targetSpeed = rigid.velocity.magnitude;

        //自分から見た相対速度を算出
        float relativeSpeed = (targetSpeed - mySpeed);

        //接近時間を算出
        float catchUpTime = relativeDistance / relativeSpeed;

        //迎撃地点を算出
        Vector3 interceptPos = targetPos + rigid.velocity * catchUpTime;

        //迎撃地点に向かわせる
        agent.SetDestination(interceptPos);
    }

    public void Escape()
    {
        //プレイヤーの座標を取得
        Vector3 targetPos = GameObject.Find("Player").transform.position;

        //この座標へ移動する
        agent.SetDestination(targetPos * -1);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //GenerateParticle();
            Destroy(this.gameObject);
        }
    }

    public void GenerateParticle()
    {
        //パーティクルエフェクトを生み出す
        GameObject pp = Instantiate(particlePrefab) as GameObject;

        //敵の位置にパーティクルを置く
        pp.transform.position = gameObject.transform.position;

        //エフェクト発生後パーティクル削除
        Destroy(pp, 2.0f);
    }
}
